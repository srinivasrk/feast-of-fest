package com.example.Fest;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.text.style.ClickableSpan;
import android.view.View;

/**
 * Created by kulkas24 on 8/31/2015.
 */
public class myClickableSpan extends ClickableSpan {

    int pos;
    private final Activity context;
    private final String contact;
    public myClickableSpan(String contact,Activity context){
        this.contact= contact;
        this.context = context;

    }

    @Override
    public void onClick(View widget) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + contact));
        Activity activity = context;
        activity.startActivity(intent);
      //  Toast.makeText(context, "working", Toast.LENGTH_LONG).show();
    }
}
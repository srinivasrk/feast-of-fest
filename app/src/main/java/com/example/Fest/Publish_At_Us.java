package com.example.Fest;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by kulkas24 on 9/1/2015.
 */
public class Publish_At_Us extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.publish_at_us);
        TextView Header = (TextView) findViewById(R.id.publish_header);
        TextView para1 = (TextView) findViewById(R.id.publish_para1);
        TextView id = (TextView) findViewById(R.id.publish_id);

        Typeface font = Typeface.createFromAsset(getAssets(), "Font/Main_Font.ttf");
        final Typeface textFont = Typeface.createFromAsset(getAssets(), "Font/Text_Font1.ttf");

        Header.setTypeface(font);
        para1.setTypeface(textFont);

        id.setTypeface(textFont);
        id.setPaintFlags(id.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"support@androsemantic.in"});
                intent.putExtra(Intent.EXTRA_SUBJECT, "Publish with us");
                intent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(intent, ""));
            }
        });

    }
}

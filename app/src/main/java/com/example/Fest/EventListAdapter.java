package com.example.Fest;

/**
 * Created by kulkas24 on 11/24/2015.
 */

    import android.content.Context;
    import android.graphics.Typeface;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.view.ViewGroup;
    import android.widget.*;


    import java.util.ArrayList;

    /**
     * Display friend list
     *
     * @author eranga herath(erangaeb@gmail.com)
     */
    public class EventListAdapter  extends BaseAdapter implements Filterable {

        private CurrentEventsFragment activity;
        private AllEventsFragment  activity1;
        private FriendFilter friendFilter;
        private Typeface typeface;
        private ArrayList<String> friendList;
        private ArrayList<String> filteredList;

        /**
         * Initialize context variables
         *  @param activity   friend list activity
         * @param friendList friend list*/
        public EventListAdapter(AllEventsFragment activity, ArrayList<String> friendList) {
            this.activity1 = activity;
            this.friendList = friendList;
            this.filteredList = friendList;
            //   typeface = Typeface.createFromAsset(activity.getAssets(), "fonts/vegur_2.otf");

            getFilter();
        }
        public EventListAdapter(CurrentEventsFragment activity, ArrayList<String> friendList) {
            this.activity = activity;
            this.friendList = friendList;
            this.filteredList = friendList;
            //   typeface = Typeface.createFromAsset(activity.getAssets(), "fonts/vegur_2.otf");

            getFilter();
        }
        /**
         * Get size of user list
         *
         * @return userList size
         */
        @Override
        public int getCount() {
            return filteredList.size();
        }

        /**
         * Get specific item from user list
         *
         * @param i item index
         * @return list item
         */
        @Override
        public Object getItem(int i) {
            return filteredList.get(i);
        }

        /**
         * Get user list item id
         *
         * @param i item index
         * @return current item id
         */
        @Override
        public long getItemId(int i) {
            return i;
        }

        /**
         * Create list row view
         *
         * @param position index
         * @param view     current list item view
         * @param parent   parent
         * @return view
         */
        @Override
        public View getView(int position, View view, ViewGroup parent) {
            // A ViewHolder keeps references to children views to avoid unnecessary calls
            // to findViewById() on each row.
            final ViewHolder holder;
            final String user = (String) getItem(position);
            return view;
        }

        /**
         * Get custom filter
         *
         * @return filter
         */
        @Override
        public Filter getFilter() {
            if (friendFilter == null) {
                friendFilter = new FriendFilter();
            }

            return friendFilter;
        }

        /**
         * Keep reference to children view to avoid unnecessary calls
         */
        static class ViewHolder {
            TextView iconText;
            TextView name;
        }

        /**
         * Custom filter for friend list
         * Filter content in friend list according to the search text
         */
        private class FriendFilter extends Filter {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null && constraint.length() > 0) {
                    ArrayList<String> tempList = new ArrayList<String>();

                    // search content in friend list
                    for (String user : friendList) {
                        if (user.contains(constraint.toString().toLowerCase())) {
                            tempList.add(user);
                        }
                    }

                    filterResults.count = tempList.size();
                    filterResults.values = tempList;
                } else {
                    filterResults.count = friendList.size();
                    filterResults.values = friendList;
                }

                return filterResults;
            }

            /**
             * Notify about filtered list to ui
             *
             * @param constraint text
             * @param results    filtered result
             */
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredList = (ArrayList<String>) results.values;
                notifyDataSetChanged();
            }
        }

    }

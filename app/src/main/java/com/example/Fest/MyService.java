package com.example.Fest;

/**
 * Created by kulkas24 on 9/9/2015.
*/

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;
import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyService extends Service {
//set current check for 10 sec
    private JSONParser jParser = new JSONParser();
    private static String url_all_events = "http://androsemantic.in/festPhp/getallevents.php";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "events";
    private static final String TAG_FEST_NAME = "fest_name";
    private static final String TAG_COLL_NAME = "coll_name";
    JSONArray products = null;
    private ArrayList<HashMap<String, String>> EventList;
    private SharedPreferences mPrefs;
    private String NumberOfEvents = "Number of Events";
    private int totalEvents;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        EventList = new ArrayList<HashMap<String, String>>();

      //       Toast.makeText(this, " MyService Created ", Toast.LENGTH_LONG).show();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //TODO do something useful
        EventList.clear();
     //   Toast.makeText(this,"Service Triggered",Toast.LENGTH_LONG).show();
        new LoadCurrentEvents().execute();
        return Service.START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
     //   Toast.makeText(this, "Servics Stopped", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }

    class LoadCurrentEvents extends AsyncTask<String, String, String> {

        SharedPreferences.Editor editor = mPrefs.edit();
        @Override
        protected String doInBackground(String... args) {
            EventList.clear();
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_all_events, "POST", params);
            if(json == null) {
                return "null";
            }

            else {
                // Check your log cat for JSON reponse
                Log.d("All Events: ", json.toString());

                try {
                    // Checking for SUCCESS TAG
                    int success = json.getInt(TAG_SUCCESS);

                    if (success == 1) {
                        // products found
                        // Getting Array of Products
                        products = json.getJSONArray(TAG_PRODUCTS);

                        // looping through All Products
                        for (int i = 0; i < products.length(); i++) {
                            JSONObject c = products.getJSONObject(i);

                            // Storing each json item in variable
                            String id = c.getString(TAG_FEST_NAME);
                            String name = c.getString(TAG_COLL_NAME);

                            // creating new HashMap
                            HashMap<String, String> map = new HashMap<String, String>();

                            // adding each child node to HashMap key => value
                            map.put(TAG_FEST_NAME, id);
                            map.put(TAG_COLL_NAME, name);

                            // adding HashList to ArrayList
                            EventList.add(map);

                        }
                        totalEvents = mPrefs.getInt(NumberOfEvents, 0);
                      /*  Handler handler =  new Handler(getApplicationContext().getMainLooper());
                        handler.post( new Runnable(){
                            public void run(){
                                Toast.makeText(getApplicationContext(), String.valueOf(totalEvents)+" : "+EventList.size(),Toast.LENGTH_LONG).show();
                            }
                        });*/
                        if (EventList.size() > totalEvents) {
                            editor.putInt(NumberOfEvents, EventList.size());
                            editor.commit();
                            String num = String.valueOf(EventList.size() - totalEvents);
                            long when = System.currentTimeMillis();
                            int icon = R.drawable.app_logo;
                            NotificationManager notificationManager = (NotificationManager)
                                    getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                            Notification notification = new Notification(icon, num+" New Events Found", when);
                            String title = "Feast of Fest";
                            Intent notificationIntent = new Intent(getApplicationContext(), Splash.class);
                            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                    Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            PendingIntent intent =
                                    PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, 0);
                            if(Integer.parseInt(num) >1)
                            {
                             //   notification.setLatestEventInfo(getApplicationContext(), title, " Woohoo !!! "+num+" New Events Found ! Check it out !!", intent);
                            }
                            else
                            {
                              //  notification.setLatestEventInfo(getApplicationContext(), title, " Woohoo !!! "+num+" New Event Found ! Check it out !!", intent);
                            }

                          //  notification.flags |= Notification.FLAG_AUTO_CANCEL;
                         //   notification.defaults |= Notification.DEFAULT_SOUND;
                          //  notification.defaults |= Notification.DEFAULT_VIBRATE;
                          //  notificationManager.notify(0, notification);

                        }
                    } else {
                        // no Events found
                        return "came here";

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }


    }


}
package com.example.Fest;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Srinivas on 6/26/2016.
 */
public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    protected TextView collegeName;
    protected TextView textView;
    protected Context viewContext;
    protected ImageView festImageIcon;
    private static final String TAG_FEST_NAME = "fest_name";
    public CustomViewHolder(View view, Context mContext) {
        super(view);
        this.collegeName = (TextView) view.findViewById(R.id.collegeName);
        this.textView = (TextView)view.findViewById(R.id.title);
        this.festImageIcon = (ImageView) view.findViewById(R.id.festIcon);
        viewContext = mContext;
        view.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        // getting values from selected ListItem
        String fest_name = ((TextView) view.findViewById(R.id.title)).getText().toString();
      //  String coll_name = ((TextView) view.findViewById(R.id.collegeName)).getText().toString();
        // Toast.makeText(getApplicationContext(), "Came here", Toast.LENGTH_LONG).show();
        // Starting new intent
       // Toast.makeText(viewContext,coll_name.toString(),Toast.LENGTH_LONG).show();
        Intent in = new Intent(viewContext,
                EventsDetails.class);
        // sending pid to next activity
        in.putExtra(TAG_FEST_NAME, fest_name);

        // starting new activity
        viewContext.startActivity(in);
         //getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}


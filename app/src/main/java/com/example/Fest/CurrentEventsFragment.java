package com.example.Fest;

import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.*;
import android.widget.*;

import com.mikepenz.materialdrawer.DrawerBuilder;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.support.v7.widget.RecyclerView;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by kulkas24 on 9/21/2015.
 */
public class CurrentEventsFragment extends Fragment implements android.support.v7.widget.SearchView.OnQueryTextListener {
    ArrayList<HashMap<String, String>> EventList;
    private ProgressDialog pDialog;
    JSONArray products = null;
    public ListView lv;
    private MenuItem searchMenuItem;
    private android.support.v7.widget.SearchView searchView;
    private EventListAdapter friendListAdapter;
    private ListView friendListView;
    private String serverURL = "http://androsemantic.in";

    private ArrayList<String> friendList = new ArrayList<String>();
    private ArrayList<String> friendListColl = new ArrayList<String>();
    private ArrayList<HashMap<String, String>> tempList;
    JSONParser jParser = new JSONParser();

    private static String url_current_events = "http://androsemantic.in/festPhp/getcurrentevents.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "events";
    private static final String TAG_FEST_NAME = "fest_name";
    private static final String TAG_COLL_NAME = "coll_name";
    private SharedPreferences sharedPreferences;
    private RecyclerView mRecyclerView;
    private Context context;
    private View rootView;
    private boolean isClosed;
    private List<FeedItem> feedsList = new ArrayList<>();
    private MyRecyclerAdapter adapter;
    private Bitmap image;
    private Bitmap mIcon11;
    private Bitmap myBitmap;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_current_event, container, false);
        context = rootView.getContext();
        EventList = new ArrayList<HashMap<String, String>>();

// Initialize recycler view
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        getActivity().startService(new Intent(context, MyService.class));
        Calendar cal = Calendar.getInstance();
        Intent intent = new Intent(context, MyService.class);
        PendingIntent pintent = PendingIntent
                .getService(context, 0, intent, 0);

        AlarmManager alarm = (AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);
        // Start service every 10 seconds
     //   alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
    //            86400 * 1000, pintent);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                3600*1000, pintent);

       // initFriendList();
        new LoadCurrentEvents().execute();

        return rootView;
    }
   /* private void initFriendList() {

        friendListAdapter = new EventListAdapter(this, friendList);
       // Toast.makeText(getActivity().getApplicationContext(),friendListAdapter.toString(),Toast.LENGTH_LONG).show();
        friendListView = (ListView) rootView.findViewById(R.id.list);

        // add header and footer for list
        //   View headerView = View.inflate(this, R.layout.list_header, null);
        //   View footerView = View.inflate(this, R.layout.list_header, null);
        //    friendListView.addHeaderView(headerView);
        ///    friendListView.addFooterView(footerView);
        friendListView.setAdapter(friendListAdapter);
        friendListView.setTextFilterEnabled(false);
    }*/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuInflater inflater1 = getActivity().getMenuInflater();
        inflater1.inflate(R.menu.menu, menu);
        inflater1.inflate(R.menu.search, menu);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        searchMenuItem = menu.findItem(R.id.search);
        searchView = ( android.support.v7.widget.SearchView) MenuItemCompat.getActionView(searchMenuItem);

        //   searchView.setIconifiedByDefault(true);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);
        isClosed = true;
    }
   /* @Override
    public boolean onQueryTextSubmit(final String query) {


      *//*  for (String t : friendList)
        {
            String temp = t.toLowerCase();
            String q = query.toLowerCase();
            if(temp.contains(q) || temp.equalsIgnoreCase(q))
                flag=1;
        }*//*
       *//* if(flag!=1)
        {
          //  Toast.makeText(getActivity().getApplicationContext(),"triggered",Toast.LENGTH_LONG).show();
            for(String t : friendListColl) {
                String temp = t.toLowerCase();
                String q = query.toLowerCase();
                if (temp.contains(q) || temp.equalsIgnoreCase(q))
                    flag = 1;
            }
        }*//*

        //Toast.makeText(getActivity().getApplicationContext(),"Working",Toast.LENGTH_LONG).show();
        tempList = new ArrayList<HashMap<String, String>>();

        try {

            for (int i = 0; i < products.length(); i++) {
                JSONObject c = products.getJSONObject(i);

                // Storing each json item in variable
                String id = c.getString(TAG_FEST_NAME);
                String collid = c.getString(TAG_COLL_NAME);
                String temp = id.toLowerCase();
                String temp2 = collid.toLowerCase();
                String q = query.toLowerCase();
                if (temp.contains(q) || temp.equalsIgnoreCase(q)) {
                    String name = c.getString(TAG_COLL_NAME);
                    //Toast.makeText(getActivity().getApplicationContext(),"working here1",Toast.LENGTH_LONG).show();
                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    map.put(TAG_FEST_NAME, id);
                    map.put(TAG_COLL_NAME, name);

                    // adding HashList to ArrayList
                    tempList.add(map);

                    FeedItem item = new FeedItem();

                    item.setTitle(id);
                    item.setThumbnail(name);

                } else if (temp2.contains(q) || temp2.equalsIgnoreCase(q)) {
                    String name = c.getString(TAG_COLL_NAME);
                    //Toast.makeText(getActivity().getApplicationContext(),"working here1",Toast.LENGTH_LONG).show();
                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    map.put(TAG_FEST_NAME, id);
                    map.put(TAG_COLL_NAME, name);

                    // adding HashList to ArrayList
                    tempList.add(map);
                }

            }
        } catch (Exception e) {

        }

//            adapter = new SimpleAdapter(
//                    context, tempList,
//                    R.layout.list_item, new String[]{TAG_FEST_NAME,
//                    TAG_COLL_NAME},
//                    new int[]{R.id.event_name, R.id.coll_name}) {
//
//                @Override
//                public View getView(int pos, View convertView, ViewGroup parent) {
//                    View v = convertView;
//                    if (v == null) {
//                        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                        v = vi.inflate(R.layout.list_item, null);
//                    }
//
//                    //  Toast.makeText(getActivity().getApplicationContext(),"working2",Toast.LENGTH_LONG).show();
//                    TextView tv = (TextView) v.findViewById(R.id.event_name);
//                    tv.setText(tempList.get(pos).get(TAG_FEST_NAME));
//                    //  tv.setTypeface(font);
//                    TextView tvs = (TextView) v.findViewById(R.id.coll_name);
//                    tvs.setText(tempList.get(pos).get(TAG_COLL_NAME));
//                    //   tvs.setTypeface(typeNormal);
//                    if (tempList.get(pos).get(TAG_COLL_NAME).toString().indexOf("NIL") >= 0) {
//                        tvs.setText("");
//                    }
//
//                    return v;
//                }
//            };
//            ListView lv = (ListView) rootView.findViewById(R.id.list);
//            lv.setAdapter(adapter);

//            notifyAll();


    }*/

   /* @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {

            adapter = new SimpleAdapter(
                    context, EventList,
                    R.layout.list_item, new String[]{TAG_FEST_NAME,
                    TAG_COLL_NAME},
                    new int[]{R.id.event_name, R.id.coll_name}) {

                @Override
                public View getView(int pos, View convertView, ViewGroup parent) {
                    View v = convertView;
                    if (v == null) {
                        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        v = vi.inflate(R.layout.list_item, null);
                    }

                    // Toast.makeText(getActivity().getApplicationContext(),"working2",Toast.LENGTH_LONG).show();
                    TextView tv = (TextView) v.findViewById(R.id.event_name);
                    tv.setText(EventList.get(pos).get(TAG_FEST_NAME));
                    //  tv.setTypeface(font);
                    TextView tvs = (TextView) v.findViewById(R.id.coll_name);
                    tvs.setText(EventList.get(pos).get(TAG_COLL_NAME));
                    //   tvs.setTypeface(typeNormal);
                    if (EventList.get(pos).get(TAG_COLL_NAME).toString().indexOf("NIL") >= 0) {
                        tvs.setText("");
                    }

                    return v;
                }
            };
            ListView lv = (ListView) rootView.findViewById(R.id.list);
            lv.setAdapter(adapter);
        }
        else {
            friendListView.setFilterText(newText.toString());
        }


        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        switch (item.getItemId())
        {
            case R.id.menu_AboutUs:
                // Single menu item is selected do something
                // Ex: launching new activity/screen or show alert message
                Intent i = new Intent(getActivity(),About_Us.class);
                startActivity(i);
                return true;

            case R.id.menu_Publish:
                Intent i1 = new Intent(getActivity(),Publish_At_Us.class);
                startActivity(i1);
                return true;

            case R.id.menu_Exit:
                System.exit(0);

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }


    class LoadCurrentEvents extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Sending our agents to look for Events ....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "Font/Text_Font1.ttf");

        protected String doInBackground(String... args) {
            // Building Parameters

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_current_events, "POST", params);
            if (json == null) {
                pDialog.dismiss();
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setMessage("Uh Uho !! Unable to connect to internet !! Please check your connection !!")
                                .setCancelable(false)
                                .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent mStartActivity = new Intent(context, Splash.class);
                                        int mPendingIntentId = 123456;
                                        PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                                        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                                        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
                                        System.exit(0);

                                    }
                                })
                                .setNegativeButton("Quit", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        getActivity().finish();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                });

                return null;
            }
            else {
                // Check your log cat for JSON reponse
                Log.d("All Events: ", json.toString());

                try {
                    // Checking for SUCCESS TAG
                    int success = json.getInt(TAG_SUCCESS);

                    if (success == 1) {
                        products = json.getJSONArray(TAG_PRODUCTS);

                        // looping through All Products
                        for (int i = 0; i < products.length(); i++) {
                            JSONObject c = products.getJSONObject(i);

                            // Storing each json item in variable
                            String id = c.getString(TAG_FEST_NAME);
                            String name = c.getString(TAG_COLL_NAME);
                            String logo = c.getString("fest_logo");
                          //  new DownloadImageTask()
                         //           .execute(serverURL + logo);

//                            Toast.makeText(getActivity().getApplicationContext(),"working2",Toast.LENGTH_LONG).show();
                            // creating new HashMap
                            HashMap<String, String> map = new HashMap<String, String>();
                            friendList.add(id);
                            friendListColl.add(name);
                            // adding each child node to HashMap key => value
                            map.put(TAG_FEST_NAME, id);
                            map.put(TAG_COLL_NAME, name);
                            FeedItem item = new FeedItem();
                            item.setTitle(id);
                            item.setCollegeName(name);
                            try {
                                    URL url = new URL(serverURL+logo);
                                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                                    connection.setDoInput(true);
                                    connection.connect();
                                    InputStream input = connection.getInputStream();
                                     myBitmap = BitmapFactory.decodeStream(input);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    return null;
                                }
                            item.setFestIcon(myBitmap);
                            feedsList.add(item);
                            // adding HashList to ArrayList
                            EventList.add(map);
                        }
                    } else {
                        // no Events found
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(context, "No Current Fests Found", Toast.LENGTH_LONG).show();
                            }
                        });

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(),e.toString(),Toast.LENGTH_LONG).show();
                }
            }
            return null;

        }
        private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
            ImageView bmImage ;

            protected Bitmap doInBackground(String... urls) {
                String urldisplay = urls[0];
                 mIcon11 = null;
                try {
                    InputStream in = new java.net.URL(urldisplay).openStream();
                    mIcon11 = BitmapFactory.decodeStream(in);
                } catch (Exception e) {

                    Log.e("Error", e.getMessage());
                    Toast.makeText(getContext(),e.toString(),Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
                finally {
                    {

                    }
                }
              //  Toast.makeText(getContext(),mIcon11.toString(),Toast.LENGTH_LONG).show();
                return mIcon11;
            }

            protected void onPostExecute(Bitmap result) {
//                bmImage.setImageBitmap(result);
            }
        }
        /**
         * After completing background task Dismiss the progress dialog
         * *
         */
        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
          //  Toast.makeText(getActivity().getApplicationContext(),String.valueOf(feedsList.size()),Toast.LENGTH_LONG).show();
            adapter = new MyRecyclerAdapter(getContext(), feedsList);
            mRecyclerView.setAdapter(adapter);

          /*  // dismiss the dialog after getting all products
            pDialog.dismiss();
            // updating UI from Background Thread
           getActivity().runOnUiThread(new Runnable() {
               public void run() {
                   *//**
                    * Updating parsed JSON data into ListView
                    * *//*

                   adapter = new SimpleAdapter(
                           context, EventList,
                           R.layout.list_item, new String[]{TAG_FEST_NAME,
                           TAG_COLL_NAME},
                           new int[]{R.id.event_name, R.id.coll_name}) {

                       @Override
                       public View getView(int pos, View convertView, ViewGroup parent) {
                           View v = convertView;
                           if (v == null) {
                               LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                               v = vi.inflate(R.layout.list_item, null);
                           }
                           TextView tv = (TextView) v.findViewById(R.id.event_name);
                           tv.setText(EventList.get(pos).get(TAG_FEST_NAME));
                           //tv.setTypeface(font);
                           TextView tvs = (TextView) v.findViewById(R.id.coll_name);
                           tvs.setText(EventList.get(pos).get(TAG_COLL_NAME));
                           //   tvs.setTypeface(typeNormal);
                           if (EventList.get(pos).get(TAG_COLL_NAME).toString().indexOf("NIL") >= 0) {
                               tvs.setText("");
                           }
                           return v;
                       }
                   };
                   // updating listview
                   //   setListAdapter(adapter);

                   ListView lv = (ListView) getActivity().findViewById(R.id.list);
                   int[] colors = {0, 0xFF347DFF, 0}; // red for the example
                   lv.setDivider(new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, colors));
                   lv.setDividerHeight(1);
                   lv.setAdapter(adapter);

                   lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                       @Override
                       public void onItemClick(AdapterView<?> parent, View view,
                                               int position, long id) {
                           // getting values from selected ListItem
                           String fest_name = ((TextView) view.findViewById(R.id.event_name)).getText().toString();
                           // Toast.makeText(getApplicationContext(), "Came here", Toast.LENGTH_LONG).show();
                           // Starting new intent
                           Intent in = new Intent(context,
                                   EventsDetails.class);
                           // sending pid to next activity
                           in.putExtra(TAG_FEST_NAME, fest_name);

                           // starting new activity
                           startActivity(in);
                           getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                       }
                   });

               }

           });*/


        }
    }

}

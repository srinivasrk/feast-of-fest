package com.example.Fest;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.VideoView;

/**
 * Created by kulkas24 on 8/13/2015.
 */
public class Splash extends Activity
{
    private static final int RESULT = 0;
    private VideoView v1;
    private Thread thread;
    SharedPreferences mPrefs;
    final String welcomeScreenShownPref = "welcomeScreenShown";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        ImageView logo = (ImageView) findViewById(R.id.imageView);
        logo.setScaleType(ImageView.ScaleType.FIT_XY);
        // 0 means success, other means failed.
        if (!isNetworkAvailable()) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Uh Uho !! Unable to connect to internet !! Please check your connection !!")
                    .setCancelable(false)
                    .setPositiveButton("Connect to Internet", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            startActivity(new Intent(Settings.ACTION_SETTINGS));
                            finish();
                        }
                    })
                    .setNegativeButton("Quit", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    });
            AlertDialog alert = builder.create();

            Window window = alert.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();

            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;

            window.setAttributes(wlp);

            alert.show();

        } else {
            mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
            Boolean welcomeScreenShown = mPrefs.getBoolean(welcomeScreenShownPref, false);

           Intent intent;
                        if (welcomeScreenShown) {
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    // TODO: Your application init goes here.
                        Intent mInHome = new Intent(Splash.this, MyActivity.class);
                        Splash.this.startActivity(mInHome);
                        Splash.this.finish();
                    }
                }, 3000);

            } else {
                SharedPreferences.Editor editor = mPrefs.edit();
                editor.putBoolean(welcomeScreenShownPref, true);
                editor.commit();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        // TODO: Your application init goes here.
                        Intent intent1 = new Intent(Splash.this, Help_Screen1.class);
                        startActivity(intent1);
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                        finish();
                    }
                }, 3000);
                //First Time App launched, you are putting isInitialAppLaunch to false and calling create password activity.


            }
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();

    }
}
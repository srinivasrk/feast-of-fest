package com.example.Fest;

/**
 * Created by Srinivas on 6/26/2016.
 */
import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MyRecyclerAdapter extends RecyclerView.Adapter< CustomViewHolder> {
    private List<FeedItem> feedItemList;
    private Context mContext;

    public MyRecyclerAdapter(Context context, List<FeedItem> feedItemList) {
        this.feedItemList = feedItemList;
        this.mContext = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_row, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view,view.getContext());
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int i) {
        FeedItem feedItem = feedItemList.get(i);
        holder.textView.setText(Html.fromHtml(feedItem.getTitle()));
        if(feedItem.getCollegeName().equals("NIL")) {
            holder.collegeName.setText("");
        }
        else {
            holder.collegeName.setText(feedItem.getCollegeName());
        }

        holder.festImageIcon.setImageBitmap(feedItem.getFestIcon());
//        Toast.makeText(mContext,feedItem.getFestIcon().toString(),Toast.LENGTH_LONG).show();
    }


    @Override
    public int getItemCount() {
        return (null != feedItemList ? feedItemList.size() : 0);
    }
}
package com.example.Fest;

/**
 * Created by kulkas24 on 11/29/2015.
 */
public interface OnBackPressedListener {
    void onBackPressed();
}

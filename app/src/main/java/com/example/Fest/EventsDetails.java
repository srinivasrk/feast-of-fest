package com.example.Fest;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.util.Base64;
import android.util.Log;
import android.view.*;
import android.widget.*;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by kulkas24 on 4/30/2015.
 */
public class EventsDetails extends Activity {
    private ProgressDialog pDialog;
    private static String url_event_detials="http://androsemantic.in/festPhp/eventdetailsdata.php";
    private static String url_event_reg="http://androsemantic.in/festPhp/eventregistrationdata.php";
    private static String url_event_list="http://androsemantic.in/festPhp/eventselect.php";
    private String serverURL = "http://androsemantic.in";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_EVENT_DETAILS_NAME = "event_data";
    ArrayList<HashMap<String, String>> eventDataList;
    HashMap<String,String > event_data_details = new HashMap<String, String>();
    // products JSONArray
    JSONArray products = null;
    // JSON parser class
    //JSONParser jsonParser = new JSONParser();
    JSONParser jParser = new JSONParser();
    private static String fest_name;
    private static String coll_name;
    private static String organizer_name;
    private Spinner spinner;
    private static String contact_info;
    private String festParameter;
    private String logo;
    private  Boolean Expired = false;
    private Boolean NoEvents = false;
    private String fest_start_date;
    private String fest_end_date;

    private SimpleAdapter adapter;
    private ImageButton locateus;
    private String latitude;
    private String longitude;
    private String location;
    private String event_type;
    private String is_reg;
    private ImageButton share;

    private ArrayList<String> eventslist = new ArrayList<String>();
    private int start1;
    private int end1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NoEvents= false;
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        setContentView(R.layout.event_details);


        eventDataList = new ArrayList<HashMap<String, String>>();
        Intent intent = getIntent();
        festParameter = intent.getStringExtra("fest_name");
        locateus = (ImageButton)findViewById(R.id.locateUs);
        share = (ImageButton)findViewById(R.id.share);
        new LoadAllEvents().execute();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;

    }
    public boolean onOptionsItemSelected(MenuItem item)
    {

        switch (item.getItemId())
        {
            case R.id.menu_Share:
                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                // Add data to the intent, the receiving app will decide
                // what to do with it.
                if(coll_name == "NIL")
                {
                    String subject = System.getProperty("line.separator")+" Fest Details : "+ " Location : "+
                            location+System.getProperty("line.separator")+" Contact Organizer : "+ organizer_name + System.getProperty("line.separator")+
                            " Contact info : "+contact_info+System.getProperty("line.separator")+
                            "  Fest Dates : "+fest_start_date+" - "+fest_end_date+System.getProperty("line.separator")+
                            " For more details download the APP : https://play.google.com/store/apps/details?id=com.androsemantic.fest  ";
                    share.putExtra(Intent.EXTRA_SUBJECT,"Feast Of Fest Presents : "+fest_name );
                    share.putExtra(Intent.EXTRA_TEXT, "Check out this Event : "+ subject );
                }
                else {
                    String subject = System.getProperty("line.separator") + " Fest Details : " + coll_name + System.getProperty("line.separator") + " Location : " +
                            location + System.getProperty("line.separator") + " Contact Organizer : " + organizer_name + System.getProperty("line.separator") +
                            " Contact info : " + contact_info + System.getProperty("line.separator") +
                            "  Fest Dates : " + fest_start_date + " - " + fest_end_date + System.getProperty("line.separator") +
                            " For more details download the APP : https://play.google.com/store/apps/details?id=com.androsemantic.fest  ";
                    share.putExtra(Intent.EXTRA_SUBJECT,"Feast Of Fest Presents : "+fest_name );
                    share.putExtra(Intent.EXTRA_TEXT, "Check out this Event : "+ subject );
                }


               startActivity(Intent.createChooser(share, "Share link!"));
                return true;

            case R.id.menu_AboutUs:
                // Single menu item is selected do something
                // Ex: launching new activity/screen or show alert message
                Intent i = new Intent(EventsDetails.this,About_Us.class);
                startActivity(i);
                return true;

            case R.id.menu_Publish:
                Intent i1 = new Intent(EventsDetails.this,Publish_At_Us.class);
                startActivity(i1);
                return true;

            case R.id.menu_Exit:
                Intent intent = new Intent(getApplicationContext(), MyActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);

            default:
                return true;
        }
    }
    class LoadAllEvents extends AsyncTask<String, String, String> {


        private JSONObject json2;


        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(EventsDetails.this);
            pDialog.setMessage("Digging more info about this event ... Please wait ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("festParameter", festParameter));
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_event_detials, "POST", params);
            if(json == null)
            {
                pDialog.dismiss();
                return "no return";
            }
            else {
                // Check your log cat for JSON reponse
                Log.d("All Events: ", json.toString());

                try {
                    // Checking for SUCCESS TAG
                    int success = json.getInt(TAG_SUCCESS);

                    if (success == 1) {
                        // products found
                        // Getting Array of Products
                        fest_name = json.getString("fest_name");
                        coll_name = json.getString("coll_name");
                        organizer_name = json.getString("organizer_name");
                        event_type = json.getString("event_data");
                        contact_info = json.getString("contact_info");
                        location = json.getString("college_addr");
                        fest_start_date = json.getString("event_start_date");
                        fest_end_date = json.getString("event_end_date");
                        logo = json.getString("fest_logo");
                        latitude = json.getString("lat");
                        longitude = json.getString("long");
                        new DownloadImageTask((ImageView) findViewById(R.id.logoImage))
                                .execute(serverURL + logo);
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }


            }


            String[] temp1 = organizer_name.split(",");
            String[] temp2 = contact_info.split(",");
            String temp="";
            String tempt="";
            for(int i=0;i<temp1.length;i++)
            {
                temp += "<a style=\"text-align:justify;margin: 0 auto;width: 10em;font-size:15px\">"+ temp1[i]+" : "+temp2[i]+"</a>";
                if(i+1==temp1.length){

                }
                else
                    temp+="<br/>";
                tempt+="<br/>"+temp1[i];
            }
            contact_info=temp;
            organizer_name=tempt;
            params.add(new BasicNameValuePair("festParameter",festParameter));
            // getting JSON string from URL
            JSONObject json3 = jParser.makeHttpRequest(url_event_list, "POST", params);
            if(json == null)
            {
                Toast.makeText(getApplicationContext(),"Unable to get event details",Toast.LENGTH_LONG).show();
            }
            else {
                try {

                    int success = json3.getInt(TAG_SUCCESS);
                    if(success ==1)
                    {
                        JSONArray eventslistdata = json3.getJSONArray("events_name");
                        for(int i=0;i<eventslistdata.length();i++)
                        {
                            if(eventslistdata.getString(i)!=null) {
                                eventslist.add(eventslistdata.getString(i));
                            }

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }

            json2 = jParser.makeHttpRequest(url_event_reg, "POST", params);
            if(json2 == null)
            {
                pDialog.dismiss();
                return "no return";
            }

            else {
                Log.d("All Register Data: ", json2.toString());

                try {
                    // Checking for SUCCESS TAG
                    int success = json2.getInt(TAG_SUCCESS);

                    if (success == 1) {

                        JSONArray eventsdata = json2.getJSONArray("data");
                        JSONArray eventsdatadetails = json2.getJSONArray("datadetails");
                        JSONArray eventOneLineDesc = json2.getJSONArray("one_line_desc");
                        JSONArray is_reg =json2.getJSONArray("isReg");
                        for (int i = 0; i < eventsdata.length(); i++) {
                            String eventname = eventsdata.getString(i);
                            String isreg = is_reg.getString(i);
                            HashMap<String, String> map = new HashMap<String, String>();
                           // map.put(TAG_EVENT_DETAILS_NAME, eventname);
                            map.put("Register Button", "Register Now");
                            map.put("One Line Desc", eventOneLineDesc.getString(i));
                            map.put("is registrable",isreg);
                            eventDataList.add(map);
                            event_data_details.put(eventname, eventsdatadetails.getString(i));

                        }
                        if(event_data_details.isEmpty())
                        {
                            NoEvents = true;
                            return "success";
                        }
                        else
                        {
                            NoEvents = false;
                            return "success";
                        }
                    }
                    else
                    {
                        NoEvents = true;
                        return "success";
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                    return "success";
                }
            }



        }

        protected void onPostExecute(String result) {
        if(result.equalsIgnoreCase("no return")) {
            pDialog.dismiss();
            Toast.makeText(getApplicationContext(), "Please check your internet connection and Try Again" +
                    "!!", Toast.LENGTH_LONG).show();
            finish();
            return;

        }
          //  final Typeface textFont = Typeface.createFromAsset(getAssets(), "Font/Text_Font1.ttf");
            // dismiss the dialog after getting all products
            pDialog.dismiss();
            try {
                String[] safe = logo.split("=");
                //put all chars before first base64 '=' padding char into safe[0]
                byte[] ba2 = Base64.decode(safe[0], Base64.NO_PADDING);
                //byte[] ba2 = Base64.decode(logo, Base64.DEFAULT);
                InputStream is = new ByteArrayInputStream(ba2);
                Bitmap image = BitmapFactory.decodeStream(is);
                ImageView logo = (ImageView) findViewById(R.id.logoImage);
                //  logo.setImageResource(d);



                logo.setImageBitmap(image);
            }
            catch (Exception e) {

            }


            //  int imgID = getResources().getIdentifier("varchasva_logo","drawable",getPackageName());


            //   Toast.makeText(getApplicationContext(),fest_name+":"+coll_name+":"+organizer_name+":"+event_data,Toast.LENGTH_LONG).show();
            TextView festName = (TextView) findViewById(R.id.festName);
            festName.setText(fest_name);
          //  festName.setTypeface(textFont);

            TextView collName = (TextView) findViewById(R.id.collName);
            collName.setText("College  : " + coll_name);
           // collName.setTypeface(textFont);

            if(coll_name.indexOf("NIL") >=0) {
                collName.setVisibility(View.GONE);
                collName.setText("");

            }
            TextView collAddr = (TextView)findViewById(R.id.locationPlace);
            collAddr.setText("Location : "+ location);
         //   collAddr.setTypeface(textFont);

            TextView organizerName = (TextView) findViewById(R.id.organizerName);
            organizerName.setGravity(Gravity.CENTER);
            organizerName.setText("Organizer : " + Html.fromHtml(organizer_name));
            organizerName.setVisibility(View.GONE);
           // organizerName.setTypeface(textFont);

            final TextView contactInfo = (TextView) findViewById(R.id.contactdetailsText);
            contactInfo.setGravity(Gravity.CENTER);
            contactInfo.setText(Html.fromHtml(contact_info), TextView.BufferType.SPANNABLE);

         //   contactInfo.setTypeface(textFont);
//            contactInfo.setTextColor(Color.parseColor("#FF0035FF"));
//            contactInfo.setPaintFlags(contactInfo.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

//            Spannable span = (Spannable)contactInfo.getText();
//            span.setSpan(new myClickableSpan(contactInfo.getText().toString(),EventsDetails.this),0,contactInfo.getText().toString().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            String regex = "\\d{10}";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(contactInfo.getText());

            Spannable s12 = (Spannable)contactInfo.getText();

            try {
                while (matcher.find()) {
                    int start = matcher.start();
                    int end = matcher.end();
                    //s1.setSpan(new ForegroundColorSpan(0xFF347DFF), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    s12.setSpan(new myClickableSpan(contactInfo.getText().toString().substring(start, end), EventsDetails.this), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                }
                contactInfo.setText(s12);
                contactInfo.setTextSize(19);
                contactInfo.setMovementMethod(LinkMovementMethod.getInstance());
            }catch (Exception e){
                Toast.makeText(getApplicationContext(),"Font Not Set !! Device not supported...",Toast.LENGTH_LONG).show();
            }



//            contactInfo.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    String regex = "\\d{10}";
//                    Pattern pattern = Pattern.compile(regex);
//                    Matcher matcher = pattern.matcher(contactInfo.getText());
//                    if(matcher.matches())
//                    {
//                            Intent intent = new Intent(Intent.ACTION_DIAL);
//                            intent.setData(Uri.parse("tel:" + contact_info));
//                            startActivity(intent);
//
//                    }
//                    else {
//                    try {
//                        Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse(contactInfo.getText().toString()));
//                        startActivity(viewIntent);
//                    }
//                    catch (Exception e)
//                    {
//                        Toast.makeText(getApplicationContext(), "Missing or Wrong URL !!! Contact our team for more help.", Toast.LENGTH_LONG).show();
//                    }
//                    }
//                }
//            });


            String[] temp = fest_start_date.split("-");
            String s1 = temp[2].toString() + "/" + temp[1].toString() + "/" + temp[0].toString();

            TextView festStartDate = (TextView) findViewById(R.id.festStartDate);
            festStartDate.setText("Fest Start Date : " + s1);
          //  festStartDate.setTypeface(textFont);

            String[] temp2 = fest_end_date.split("-");
            String s2 = temp2[2].toString() + "/" + temp2[1].toString() + "/" + temp2[0].toString();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            String currentDateandTime = sdf.format(new Date());
            try {
                String d1 = temp2[0]+temp2[1]+temp2[2];
                 Date festEndDateCompare = sdf.parse(d1);
                 Date currdatecompare = sdf.parse(currentDateandTime);
                 if(festEndDateCompare.before(currdatecompare))
                     {
                       Expired = true;
                         EventsDetails.this.runOnUiThread(new Runnable() {
                             public void run() {
                                 Toast.makeText(getApplicationContext(), "This Event seems to have ended or Registration has been closed!! Try next year :)", Toast.LENGTH_LONG).show();
                             }
                         });
                     }
                else
                 {
                     Expired = false;
                 }
                }
            catch (Exception e)
                {
                    e.printStackTrace();
                }

            TextView festEndDate = (TextView) findViewById(R.id.festEndDate);
            festEndDate.setText("Fest End Date : " + s2);
           // festEndDate.setTypeface(textFont);

            locateus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?daddr="+latitude+","+longitude));
                    startActivity(intent);
                }
            });

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent share = new Intent(android.content.Intent.ACTION_SEND);
                    share.setType("text/plain");
                    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    share.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    // Add data to the intent, the receiving app will decide
                    // what to do with it.
                    if(coll_name == "NIL")
                    {
                        String subject = System.getProperty("line.separator")+"Fest name:"+fest_name+System.getProperty("line.separator")+
                                "Fest Dates : "+fest_start_date+" - "+fest_end_date+System.getProperty("line.separator")+
                                " For more details download the APP : https://play.google.com/store/apps/details?id=com.androsemantic.fest  ";
                        share.putExtra(Intent.EXTRA_SUBJECT,"Feast Of Fest Presents : "+fest_name );
                        share.putExtra(Intent.EXTRA_TEXT, "Check out this Event : "+ subject );
                    }
                    else {
                        String subject = System.getProperty("line.separator") + " Fest Details : "+ fest_name+System.getProperty("line.separator")+ "" +
                                " Fest Dates : " + fest_start_date + " - " + fest_end_date + System.getProperty("line.separator") +
                                " For more details download the APP : https://play.google.com/store/apps/details?id=com.androsemantic.fest  ";
                        share.putExtra(Intent.EXTRA_SUBJECT,"Feast Of Fest Presents : "+fest_name );
                        share.putExtra(Intent.EXTRA_TEXT, "Check out this Event : "+ subject );
                    }


                    startActivity(Intent.createChooser(share, "Share link!"));
                }
            });
    if(NoEvents == false) {
        try {

            final TextView eventData = (TextView) findViewById(R.id.eventData);
            eventData.setText(":  Event Details  : ");

            //  eventData.setTypeface(textFont);

//            for (int i = 0; i <= tempeventdata.length-1 ; i++) {
//
//                HashMap<String, String> map = new HashMap<String, String>();
//                map.put(TAG_EVENT_DETAILS_NAME, tempeventdata[i]);
//                map.put("Register Button", "Register Now");
//                eventDataList.add(map);
//
//            }


            final TextView tv1 = (TextView) findViewById(R.id.eventDataReg);
            tv1.setText(fest_name);
            tv1.setVisibility(View.VISIBLE);
            //    tv.setTypeface(textFont);


            Button tvs = (Button) findViewById(R.id.registerNow);
            tvs.setText("Register Now");
            tvs.setVisibility(View.VISIBLE);
            //  tvs.setTypeface(textFont);
            if (Expired == true) {
                tvs.setEnabled(false);
            }


            TextView tv2 = (TextView) findViewById(R.id.lineDesc);
            tv2.setVisibility(View.VISIBLE);
           String s = json2.getJSONArray("one_line_desc").getString(0);
            tv2.setText(Html.fromHtml(s), TextView.BufferType.SPANNABLE);


                         /*   tv2.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {
                                    int action = event.getAction();
                                    switch (action) {
                                        case MotionEvent.ACTION_DOWN:
                                            // Disallow ScrollView to intercept touch events.
                                           // Toast.makeText(getApplicationContext(),"DOWN",Toast.LENGTH_SHORT).show();
                                            findViewById(R.id.eventRegListview).getParent().requestDisallowInterceptTouchEvent(true);
                                            break;

                                        case MotionEvent.ACTION_UP:
                                            // Allow ScrollView to intercept touch events.
                                         //   Toast.makeText(getApplicationContext(),"UP",Toast.LENGTH_SHORT).show();
                                            findViewById(R.id.eventRegListview).getParent().requestDisallowInterceptTouchEvent(false);
                                            break;
                                    }

                                    // Handle ListView touch events.
                                    v.onTouchEvent(event);
                                    return true;
                                }
                            });*/
            final String temp1 = json2.getJSONArray("one_line_desc").getString(0);
            // tv2.setText(eventDataList.get(pos).get("One Line Desc"));
            //  tv2.setTypeface(textFont);
//
           final int start = tv2.getText().toString().indexOf("click here :");
           /* if (start != -1) {
                tv2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent viewIntent = new Intent("android.intent.action.VIEW", Uri.parse(temp1.substring(start + 13, temp1.length())));
                        startActivity(viewIntent);
                    }
                });


            }*/


             regex = "http://|https://";
            pattern = Pattern.compile(regex);
            matcher = pattern.matcher(tv2.getText());

            Spannable newSpan = (Spannable)tv2.getText();

            try {
                while (matcher.find()) {
                    start1 = matcher.start();
                    end1 = matcher.end();
                    //s1.setSpan(new ForegroundColorSpan(0xFF347DFF), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    newSpan.setSpan(new URLSpan(tv2.getText().toString().substring(start1, tv2.length())), start1, tv2.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                }
                tv2.setText(newSpan);
                tv2.setTextSize(19);
                tv2.setMovementMethod(LinkMovementMethod.getInstance());
            }catch (Exception e){
                Toast.makeText(getApplicationContext(),"Font Not Set !! Device not supported...",Toast.LENGTH_LONG).show();
            }










         //   adapter.notifyDataSetChanged();


        /*    View itemView = super.getView(pos, convertView, parent);
            View itemData = super.getView(pos, convertView, parent);

            View myTaskButton = itemView.findViewById(R.id.registerNow);
            final View myitemData = itemView.findViewById(R.id.eventDataReg);*/
            // Toast.makeText(getApplicationContext(),eventDataList.get(pos).get("is registrable").toString(),Toast.LENGTH_LONG).show();

            if (eventDataList.get(0).get("is registrable").equalsIgnoreCase("no")) {
                tvs.setEnabled(false);
            }


            tvs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        LayoutInflater factory = LayoutInflater.from(getApplicationContext());
                        final View selectDialogView = factory.inflate(
                                R.layout.select_events, null);
                        final AlertDialog selectDialog = new AlertDialog.Builder(EventsDetails.this,R.style.Theme_D1NoTitleDim).create();
                        selectDialog.setView(selectDialogView);
                      // selectDialogView.setBackgroundResource(R.color.Black);
                        TextView t1 = (TextView)selectDialogView.findViewById(R.id.festName_select);
                        t1.setText("Select the event you want to register to");
                       spinner = (Spinner)selectDialogView.findViewById(R.id.events_list_spinner);
                         String[] mServices = eventslist.toArray(new String[eventslist.size()]);
                       ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(EventsDetails.this, android.R.layout.simple_spinner_item, mServices);
                        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                       spinner.setAdapter(spinnerAdapter);
                    //    spinnerAdapter.notifyDataSetChanged();
                        selectDialog.show();

                        selectDialogView.findViewById(R.id.selectEvent_button).setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                //your business logic

                                String event_name = spinner.getSelectedItem().toString();

                                String temp_event_data_details = event_data_details.get(event_name);

                                Intent i = new Intent(getApplicationContext(), RegisterEvent.class);
                                i.putExtra("fest_name", fest_name);

                                i.putExtra("event_name", event_name);
                                i.putExtra("event_data", temp_event_data_details);
                                i.putExtra("ref_coll_name", coll_name);
                                startActivity(i);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                selectDialog.dismiss();
                            }
                        });
                        selectDialogView.findViewById(R.id.cancelEvent_button).setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                selectDialog.dismiss();

                            }
                        });


                    }
                    catch (Exception e)
                    {
                        Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
                    }
                  /*  Intent i = new Intent(getApplicationContext(), RegisterEvent.class);
                    i.putExtra("fest_name", fest_name);

                    i.putExtra("event_name", tv1.getText().toString());
                    i.putExtra("event_data", event_data_details.get(tv1.getText().toString()));
                    i.putExtra("ref_coll_name", coll_name);
                    startActivity(i);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/
                }
            });


            if (start != -1) {
                tv2.setText(Html.fromHtml(tv2.getText().toString().substring(0, start + 12) + "<font color=#7FFF00 ><u>" + tv2.getText().toString().substring(start + 13, temp1.length()) + "</u></font>"));
            } else {
                tv2.setText(Html.fromHtml(tv2.getText().toString()));
            }



//
//                            tvs.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    Intent i = new Intent(getApplicationContext(),RegisterEvent.class);
//                                    i.putExtra("fest_name",fest_name);
//                                  TextView tv1 = (TextView) findViewById(R.id.eventDataReg);
//                                    i.putExtra("event_name",tv1.getText().toString());
//                                    i.putExtra("event_data",event_data_details.get(tv1.getText().toString()));
//                                    startActivity(i);
//                                }
//                            });


        }
        catch (Exception e){


                        }

                        ;
                        //Replace the Display

                      //  ListView lv = (ListView) findViewById(R.id.eventRegListview);

                      //  int[] colors = {0, 0xFF347DFF, 0}; // red for the example
                     //   lv.setDivider(new

                   //                     GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, colors)

                  //      );
                  //      lv.setDividerHeight(1);
                  //      lv.setAdapter(adapter);
                  //      lv.setScrollContainer(false);

//                    lv.setOnTouchListener(new ListView.OnTouchListener() {
//                        @Override
//                        public boolean onTouch(View v, MotionEvent event) {
//                            int action = event.getAction();
//                            switch (action) {
//                                case MotionEvent.ACTION_DOWN:
//                                    // Disallow ScrollView to intercept touch events.
//                                  //  Toast.makeText(getApplicationContext(), "DOWN", Toast.LENGTH_SHORT).show();
//                                   // v.getParent().requestDisallowInterceptTouchEvent(true);
//                                    break;
//
//                                case MotionEvent.ACTION_UP:
//                                    // Allow ScrollView to intercept touch events.
//                                  //  Toast.makeText(getApplicationContext(), "UP", Toast.LENGTH_SHORT).show();
//                                    v.getParent().requestDisallowInterceptTouchEvent(false);
//                                    break;
//                            }
//
//                            // Handle ListView touch events.
//                            v.onTouchEvent(event);
//                            return true;
//                        }
//                    });

//                    final ListView lv1 = (ListView) findViewById(R.id.eventRegListview);
//                    lv1.setOnScrollListener(new AbsListView.OnScrollListener() {
//
//                        @Override
//                        public void onScrollStateChanged(AbsListView view, int scrollState) {
//                            // TODO Auto-generated method stub
//
//                        }
//
//                        @Override
//                        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                            if (firstVisibleItem == 0) {
//                                // check if we reached the top or bottom of the list
//                                View v = lv1.getChildAt(0);
//                                int offset = (v == null) ? 0 : v.getTop();
//                                if (offset == 0) {
//                                    findViewById(R.id.eventRegListview).getParent().requestDisallowInterceptTouchEvent(false);
//                                    return;
//                                }
//                            } else if (totalItemCount - visibleItemCount == firstVisibleItem) {
//                                View v = lv1.getChildAt(totalItemCount - 1);
//                                int offset = (v == null) ? 0 : v.getTop();
//                                if (offset == 0) {
//                                    findViewById(R.id.eventRegListview).getParent().requestDisallowInterceptTouchEvent(false);
//                                    return;
//                                }
//                            }
//                        }
//                    });
                   /* ScrollView s1 = (ScrollView)findViewById(R.id.scrollView);
                    s1.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {

                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            return false;

                        }
                    });
                    lv.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {

                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            return false;
                        }
                    });*/


//                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                        @Override
//                        public void onItemClick(AdapterView<??> parent, View view, int position, long id) {
////                            String selected_event = ((TextView) view.findViewById(R.id.eventDataReg)).getText().toString();
////                            Intent in = new Intent(getApplicationContext(),
////                                    RegisterEvent.class);
////                            in.putExtra("EventName",selected_event);
////                            startActivity(in);
//                            Toast.makeText(getApplicationContext(),"working",Toast.LENGTH_LONG).show();
//                        }
//                    });


//            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
//                    // getting values from selected ListItem
//                    String fest_name = ((TextView) view.findViewById(R.id.event_name)).getText().toString();
//                    // Toast.makeText(getApplicationContext(), "Came here", Toast.LENGTH_LONG).show();
//                    // Starting new intent
//                    Intent in = new Intent(getApplicationContext(),
//                            EventsDetails.class);
//                    // sending pid to next activity
//                    in.putExtra(TAG_FEST_NAME, fest_name);
//
//                    // starting new activity
//                    startActivity(in);
//                }
//            });


                    }




                    else
                    {
                        Button tvs = (Button) findViewById(R.id.registerNow);
                        tvs.setText("");
                        tvs.setVisibility(View.GONE);
                        TextView tv = (TextView) findViewById(R.id.eventDataReg);
                        tv.setText("");
                        tv.setVisibility(View.GONE);
                        TextView one = (TextView)findViewById(R.id.lineDesc);
                        one.setText("");
                        one.setVisibility(View.GONE);
                        final TextView eventData = (TextView) findViewById(R.id.eventData);

                        eventData.setText(Html.fromHtml("<font color=red> The fest is yet to be scheduled, Dates mentioned are tentative , Keep checking for updates. </font> "));
                        //eventData.setTypeface(textFont);
                        eventData.setTextSize(16);

                        eventData.setPaintFlags(contactInfo.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    }
                }


            }
                    // Download the logo
        private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {

                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            finally {
                {

                }
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if(listAdapter == null) return;
        if(listAdapter.getCount() <= 1) return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        int totalHeight = 0;
        View view = null;
        for(int i = 0; i < listAdapter.getCount(); i++)
        {
            view = listAdapter.getView(i, view, listView);
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

}


package com.example.Fest;

/**
 * Created by kulkas24 on 8/28/2015.
 */

import android.app.Application;
import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;


@ReportsCrashes(formUri = "https://collector.tracepot.com/05236f26",
        mode = ReportingInteractionMode.TOAST,
        forceCloseDialogAfterToast = false, // optional, default false
        resToastText = R.string.crash_toast_text)

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        // The following line triggers the initialization of ACRA
        ACRA.init(this);
        ACRA.getErrorReporter().putCustomData("TRACEPOT_DEVELOP_MODE", "1");
    }
}

package com.example.Fest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Srinivas on 6/26/2016.
 */
public class FeedItem {
    private String title;
    private String collegeName;
    private Bitmap festIcon;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collname) {
        this.collegeName = collname;
    }
    public  void setFestIcon(Bitmap image){
        festIcon=image;
    }

    public Bitmap getFestIcon(){
        return this.festIcon;
    }
}
package com.example.Fest;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by kulkas24 on 9/1/2015.
 */
public class About_Us extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.about_us);
        TextView Header = (TextView)findViewById(R.id.contact_header);
        TextView para1 = (TextView)findViewById(R.id.contact_para1);
        TextView para2 = (TextView)findViewById(R.id.contact_para2);
        TextView footer = (TextView)findViewById(R.id.contact_footer);
        TextView id = (TextView)findViewById(R.id.contact_id);
        TextView call_lbl = (TextView)findViewById(R.id.call_lbl);
        TextView callid = (TextView)findViewById(R.id.call_id);
        TextView fb = (TextView)findViewById(R.id.fb);
        ImageButton fbid = (ImageButton)findViewById(R.id.fb_id);
        Typeface font = Typeface.createFromAsset(getAssets(), "Font/Main_Font.ttf");
        final Typeface textFont = Typeface.createFromAsset(getAssets(), "Font/Text_Font1.ttf");
        Header.setTypeface(font);
        para1.setTypeface(textFont);
        para2.setTypeface(textFont);
        footer.setTypeface(textFont);
        id.setTypeface(textFont);
        callid.setTypeface(textFont);
        call_lbl.setTypeface(textFont);
        fb.setTypeface(textFont);
        fbid.setImageResource(R.drawable.fblogo);

        id.setPaintFlags(id.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        String regex = "\\d{10}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(callid.getText());
callid.setText(callid.getText(), TextView.BufferType.SPANNABLE);
        Spannable s12 = (Spannable)callid.getText();

        try {
            while (matcher.find()) {
                int start = matcher.start();
                int end = matcher.end();
                //s1.setSpan(new ForegroundColorSpan(0xFF347DFF), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                s12.setSpan(new myClickableSpan(callid.getText().toString().substring(start, end), About_Us.this), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            callid.setText(s12);
            callid.setMovementMethod(LinkMovementMethod.getInstance());
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "Font Not Set !! Device not supported...", Toast.LENGTH_LONG).show();
        }

        id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"developers@androsemantic.in"});
                intent.putExtra(Intent.EXTRA_SUBJECT, "In-APP Query");
                intent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(intent, ""));
            }
        });

        fbid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.facebook.com/FeastofFest/?fref=ts"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
    }
}

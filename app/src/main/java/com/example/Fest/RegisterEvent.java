package com.example.Fest;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.*;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by kulkas24 on 8/23/2015.
 */
public class RegisterEvent extends Activity {
    private String festParameter;
    private String eventParameter;
    private String eventData;
    private ProgressDialog pDialog;
    private EditText name;
    private EditText college;
    private EditText contact;
    private EditText usn;
    JSONParser jsonParser = new JSONParser();
    private String TAG_SUCCESS="success";
    private String url_register_event ="http://androsemantic.in/festPhp/registeruserforevent.php";
    private int success = 0;
    private SharedPreferences sharedPreferences;
    private String ref_coll_name;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.register_event);
        Intent intent = getIntent();
        festParameter = intent.getStringExtra("fest_name");
        eventParameter = intent.getStringExtra("event_name");
        eventData = intent.getStringExtra("event_data");
        ref_coll_name = intent.getStringExtra("ref_coll_name");
        final Typeface MainFont = Typeface.createFromAsset(getAssets(),"Font/Main_Font.ttf");
        final Typeface textFont = Typeface.createFromAsset(getAssets(), "Font/Text_Font1.ttf");

        TextView festNameHeading = (TextView)findViewById(R.id.festNameheading_text);
        festNameHeading.setText(festParameter);
        festNameHeading.setTypeface(MainFont);

     //  TextView festEventName = (TextView)findViewById(R.id.eventNameheading_text);
     //   festEventName.setText(eventParameter, TextView.BufferType.SPANNABLE);
     //   festEventName.setTypeface(MainFont);
     //   festEventName.setTypeface(textFont);

        final TextView festEventDetails = (TextView)findViewById(R.id.eventDetailsheading_text);
        festEventDetails.setGravity(Gravity.CENTER);
        festEventDetails.setText(Html.fromHtml(eventData), TextView.BufferType.SPANNABLE);

//        String[] temp = eventData.split("/n");
//        festEventDetails.setText(" ");
//        int i;
//        for( i=0;i<temp.length;i++) {
//            festEventDetails.setText(festEventDetails.getText()+temp[i]+"\n",TextView.BufferType.SPANNABLE);
//
//        }
//        festEventDetails.post(new Runnable() {
//            @Override
//            public void run() {
//                int lines = festEventDetails.getLineCount();
//                festEventDetails.setLines(lines);
//            }
//        });
//        ClickableSpan clickableSpan = new ClickableSpan() {
//            @Override
//            public void onClick(View textView) {
//                Toast.makeText(RegisterEvent.this, "Clicked", Toast.LENGTH_SHORT).show();
////                Intent intent = new Intent(Intent.ACTION_DIAL);
////                TextView tv1 = (TextView) textView;
////                intent.setData(Uri.parse("tel:" + tv1.getText()));
////                startActivity(intent);
//            }
//            @Override
//            public void updateDrawState(TextPaint ds) {
//                super.updateDrawState(ds);
//                ds.setUnderlineText(false);
//            }
//        };

        festEventDetails.setTypeface(textFont);

        String regex = "\\d{10}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(festEventDetails.getText());

        Spannable s1 = (Spannable)festEventDetails.getText();

try {
    while (matcher.find()) {
        int start = matcher.start();
        int end = matcher.end();
        //s1.setSpan(new ForegroundColorSpan(0xFF347DFF), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        s1.setSpan(new myClickableSpan(festEventDetails.getText().toString().substring(start, end), this), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

    }

    festEventDetails.setText(s1);

    int start = festEventDetails.getText().toString().indexOf("Participation Fee");
    int end = festEventDetails.getText().toString().indexOf("Cash Prize");

    s1.setSpan((new ForegroundColorSpan(0xffed3c13)), start, end - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    festEventDetails.setText(s1);


     start = festEventDetails.getText().toString().indexOf("Cash Prize");
     end = festEventDetails.getText().toString().indexOf("Rules");

    s1.setSpan((new ForegroundColorSpan(0xffed3c13)), start, end - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    festEventDetails.setText(s1);

    start = festEventDetails.getText().toString().indexOf("Venue");
    end = festEventDetails.getText().toString().length();
    s1.setSpan((new ForegroundColorSpan(0xffed3c13)), start, end , Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    festEventDetails.setText(s1);
    //    SpannableString s1 = new SpannableString(spannablestring[0]);
    //     s1.setSpan(clickableSpan, 0, 10, 0);
    //     s.setSpan(clickableSpan, 0, 4, 0);
    festEventDetails.setMovementMethod(LinkMovementMethod.getInstance());

}
    catch (Exception e)
    {
            Toast.makeText(getApplicationContext(),"Font Not Set !! Device not supported...",Toast.LENGTH_LONG).show();
    }

        TextView tv1 = (TextView)findViewById(R.id.tv1);
        tv1.setTypeface(textFont);
        TextView tv2 = (TextView)findViewById(R.id.tv2);
        tv2.setTypeface(textFont);
        TextView tv3 = (TextView)findViewById(R.id.tv3);
        tv3.setTypeface(textFont);
        TextView tv4 = (TextView)findViewById(R.id.tv4);
        tv4.setTypeface(textFont);

         name = (EditText)findViewById(R.id.nameInput);
        college = (EditText)findViewById(R.id.collegeInput);
         contact = (EditText)findViewById(R.id.contactInput);
         usn = (EditText)findViewById(R.id.usnInput);

        loadSavedPrefs();
        Button register = (Button)findViewById(R.id.confirmRegister);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((name.getText().length() == 0) || college.getText().length() == 0 || contact.getText().length()==0||usn.getText().length()==0) {
                    Toast.makeText(getApplicationContext(),"Please Enter all the Values",Toast.LENGTH_LONG).show();
                }
                else if(name.getText().toString().matches(".*\\d+.*") || college.getText().toString().matches(".*\\d+.*"))
                {
                    Toast.makeText(getApplicationContext(),"Enter alphabetical characters only",Toast.LENGTH_LONG).show();
                }
                else if(contact.getText().toString().length() !=10)
                {
                    Toast.makeText(getApplicationContext(),"Enter a valid 10 digit mobile number",Toast.LENGTH_LONG).show();
                }
                else if(contact.getText().toString().matches("[a-zA-Z]+"))
                {
                    Toast.makeText(getApplicationContext(),"Enter a valid contact number",Toast.LENGTH_LONG).show();
                }
                else
                {
                    new RegisterEvents().execute();

                }
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;

    }
    public boolean onOptionsItemSelected(MenuItem item)
    {

        switch (item.getItemId())
        {
            case R.id.menu_AboutUs:
                // Single menu item is selected do something
                // Ex: launching new activity/screen or show alert message
                Intent i = new Intent(RegisterEvent.this,About_Us.class);
                startActivity(i);
                return true;

            case R.id.menu_Publish:
                Intent i1 = new Intent(RegisterEvent.this,Publish_At_Us.class);
                startActivity(i1);
                return true;

            case R.id.menu_Exit:
                Intent intent = new Intent(getApplicationContext(), MyActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    class RegisterEvents extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            pDialog = new ProgressDialog(RegisterEvent.this);
            pDialog.setMessage("Registering you for this event ... Please wait !!");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(String... args) {
            String regname = name.getText().toString();
            String regcontact = contact.getText().toString();
            String regcollege = college.getText().toString();
            String regusn = usn.getText().toString();
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("name", regname));
            params.add(new BasicNameValuePair("contact", regcontact));
            params.add(new BasicNameValuePair("college", regcollege));
            params.add(new BasicNameValuePair("usn", regusn));
            params.add((new BasicNameValuePair("event_name", eventParameter)));
            params.add((new BasicNameValuePair("event_coll_name",ref_coll_name)));
            // getting JSON Object

            // Note that create product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(url_register_event,
                    "POST", params);
            if (json == null) {
                pDialog.dismiss();
                return "No return";
            } else {
                // check log cat fro response
                Log.d("Create Response", json.toString());

                // check for success tag
                try {
                    success = json.getInt(TAG_SUCCESS);

                    if (success == 1) {
                        // successfully created product
                        return "Success";

                        // closing this screen

                    } else {
                            return "No return";
                            }
                } catch (JSONException e) {
                    e.printStackTrace();
                    return "No return";
                }
            }

        }

        protected void onPostExecute(String result){
            pDialog.dismiss();
            if(result.equalsIgnoreCase("Exception Caught")){
               Toast.makeText(getApplicationContext(),"Oops... SOmething WenT Wrong !!!",Toast.LENGTH_LONG).show();
                savePreferences();
                finish();
                return;
            }else if(result.equalsIgnoreCase("Success")){
                pDialog.dismiss();
               Toast.makeText(getApplicationContext(),"Thank you for Registering !! ALL THE BEST for your event",Toast.LENGTH_LONG).show();
                savePreferences();
                finish();
                return;
            }
            if(result.equalsIgnoreCase("No return")){
                Toast.makeText(getApplicationContext(), "Uh uho .... Unable to register !! Please check internet connection !!", Toast.LENGTH_LONG).show();
                finish();
                return;
            }
            else{
                pDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Uh uho .... Unable to register !! Please check internet connection !!", Toast.LENGTH_LONG).show();
                savePreferences();
                finish();
                return;
            }
        }
    }
    @Override
    public void onBackPressed() {
        savePreferences();
        super.onBackPressed();
    }
    private void savePreferences() {
        sharedPreferences = this.getSharedPreferences("myPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("name", name.getText().toString());
        editor.putString("contact", contact.getText().toString());
        editor.putString("college", college.getText().toString());
        editor.putString("usn", usn.getText().toString());
        editor.apply();

    }

    private void loadSavedPrefs() {

         sharedPreferences = this.getSharedPreferences("myPrefs", MODE_PRIVATE);

        try {
            if (sharedPreferences.getString("name", "") != null) {
                name.setText(sharedPreferences.getString("name", ""));
            }
        }
        catch (Exception e){

        }
        try {
            if (sharedPreferences.getString("contact", "") != null) {
                contact.setText(sharedPreferences.getString("contact", ""));
            }
        }
        catch (Exception e){}
        try{
            if(sharedPreferences.getString("college","")!=null)
            {
                college.setText(sharedPreferences.getString("college",""));
            }
        }
        catch (Exception e){}
        try {
            if (sharedPreferences.getString("usn", "") != null) {
                usn.setText(sharedPreferences.getString("usn", ""));
            }
        }
        catch (Exception e) {

        }



    }

}

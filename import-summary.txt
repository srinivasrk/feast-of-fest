ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Risky Project Location:
-----------------------
The tools *should* handle project locations in any directory. However,
due to bugs, placing projects in directories containing spaces in the
path, or characters like ", ' and &, have had issues. We're working to
eliminate these bugs, but to save yourself headaches you may want to
move your project to a location where this is not a problem.
C:\Users\Srinivas\StudioProjects\Fest - Working Current
                                     - -       -       

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* .idea\
* .idea\.name
* .idea\artifacts\
* .idea\artifacts\Fest.xml
* .idea\codeStyleSettings.xml
* .idea\compiler.xml
* .idea\copyright\
* .idea\copyright\profiles_settings.xml
* .idea\dictionaries\
* .idea\dictionaries\kulkas24.xml
* .idea\inspectionProfiles\
* .idea\inspectionProfiles\Project_Default.xml
* .idea\inspectionProfiles\profiles_settings.xml
* .idea\misc.xml
* .idea\modules.xml
* .idea\uiDesigner.xml
* .idea\vcs.xml
* .idea\workspace.xml
* Feast Of Fest.apk
* Feast Of Fest.iml
* Fest.apk
* ant.properties
* build.xml
* out\
* out\production\
* out\production\Feast Of Fest\
* out\production\Feast Of Fest\Feast Of Fest.apk
* out\production\Feast Of Fest\Feast Of Fest.unaligned.apk
* out\production\Feast Of Fest\com\
* out\production\Feast Of Fest\com\example\
* out\production\Feast Of Fest\com\example\Fest\
* out\production\Feast Of Fest\com\example\Fest\About_Us$1.class
* out\production\Feast Of Fest\com\example\Fest\About_Us$2.class
* out\production\Feast Of Fest\com\example\Fest\About_Us.class
* out\production\Feast Of Fest\com\example\Fest\AllEventsFragment$1.class
* out\production\Feast Of Fest\com\example\Fest\AllEventsFragment$2.class
* out\production\Feast Of Fest\com\example\Fest\AllEventsFragment$3.class
* out\production\Feast Of Fest\com\example\Fest\AllEventsFragment$LoadAllEvents$1$1.class
* out\production\Feast Of Fest\com\example\Fest\AllEventsFragment$LoadAllEvents$1$2.class
* out\production\Feast Of Fest\com\example\Fest\AllEventsFragment$LoadAllEvents$1.class
* out\production\Feast Of Fest\com\example\Fest\AllEventsFragment$LoadAllEvents$2.class
* out\production\Feast Of Fest\com\example\Fest\AllEventsFragment$LoadAllEvents$3$1.class
* out\production\Feast Of Fest\com\example\Fest\AllEventsFragment$LoadAllEvents$3$2.class
* out\production\Feast Of Fest\com\example\Fest\AllEventsFragment$LoadAllEvents$3.class
* out\production\Feast Of Fest\com\example\Fest\AllEventsFragment$LoadAllEvents.class
* out\production\Feast Of Fest\com\example\Fest\AllEventsFragment.class
* out\production\Feast Of Fest\com\example\Fest\All_Events$LoadAllEvents$1$1.class
* out\production\Feast Of Fest\com\example\Fest\All_Events$LoadAllEvents$1$2.class
* out\production\Feast Of Fest\com\example\Fest\All_Events$LoadAllEvents$1.class
* out\production\Feast Of Fest\com\example\Fest\All_Events$LoadAllEvents$2.class
* out\production\Feast Of Fest\com\example\Fest\All_Events$LoadAllEvents$3$1.class
* out\production\Feast Of Fest\com\example\Fest\All_Events$LoadAllEvents$3$2.class
* out\production\Feast Of Fest\com\example\Fest\All_Events$LoadAllEvents$3.class
* out\production\Feast Of Fest\com\example\Fest\All_Events$LoadAllEvents.class
* out\production\Feast Of Fest\com\example\Fest\All_Events.class
* out\production\Feast Of Fest\com\example\Fest\BuildConfig.class
* out\production\Feast Of Fest\com\example\Fest\CurrentEventsFragment$1.class
* out\production\Feast Of Fest\com\example\Fest\CurrentEventsFragment$2.class
* out\production\Feast Of Fest\com\example\Fest\CurrentEventsFragment$3.class
* out\production\Feast Of Fest\com\example\Fest\CurrentEventsFragment$LoadCurrentEvents$1$1.class
* out\production\Feast Of Fest\com\example\Fest\CurrentEventsFragment$LoadCurrentEvents$1$2.class
* out\production\Feast Of Fest\com\example\Fest\CurrentEventsFragment$LoadCurrentEvents$1.class
* out\production\Feast Of Fest\com\example\Fest\CurrentEventsFragment$LoadCurrentEvents$2.class
* out\production\Feast Of Fest\com\example\Fest\CurrentEventsFragment$LoadCurrentEvents$3$1.class
* out\production\Feast Of Fest\com\example\Fest\CurrentEventsFragment$LoadCurrentEvents$3$2.class
* out\production\Feast Of Fest\com\example\Fest\CurrentEventsFragment$LoadCurrentEvents$3.class
* out\production\Feast Of Fest\com\example\Fest\CurrentEventsFragment$LoadCurrentEvents.class
* out\production\Feast Of Fest\com\example\Fest\CurrentEventsFragment.class
* out\production\Feast Of Fest\com\example\Fest\Current_Events$LoadCurrentEvents$1$1.class
* out\production\Feast Of Fest\com\example\Fest\Current_Events$LoadCurrentEvents$1$2.class
* out\production\Feast Of Fest\com\example\Fest\Current_Events$LoadCurrentEvents$1.class
* out\production\Feast Of Fest\com\example\Fest\Current_Events$LoadCurrentEvents$2.class
* out\production\Feast Of Fest\com\example\Fest\Current_Events$LoadCurrentEvents$3$1.class
* out\production\Feast Of Fest\com\example\Fest\Current_Events$LoadCurrentEvents$3$2.class
* out\production\Feast Of Fest\com\example\Fest\Current_Events$LoadCurrentEvents$3.class
* out\production\Feast Of Fest\com\example\Fest\Current_Events$LoadCurrentEvents.class
* out\production\Feast Of Fest\com\example\Fest\Current_Events.class
* out\production\Feast Of Fest\com\example\Fest\EventListAdapter$1.class
* out\production\Feast Of Fest\com\example\Fest\EventListAdapter$FriendFilter.class
* out\production\Feast Of Fest\com\example\Fest\EventListAdapter$ViewHolder.class
* out\production\Feast Of Fest\com\example\Fest\EventListAdapter.class
* out\production\Feast Of Fest\com\example\Fest\EventsDetails$DownloadImageTask.class
* out\production\Feast Of Fest\com\example\Fest\EventsDetails$LoadAllEvents$1.class
* out\production\Feast Of Fest\com\example\Fest\EventsDetails$LoadAllEvents$2.class
* out\production\Feast Of Fest\com\example\Fest\EventsDetails$LoadAllEvents$3.class
* out\production\Feast Of Fest\com\example\Fest\EventsDetails$LoadAllEvents$4$1.class
* out\production\Feast Of Fest\com\example\Fest\EventsDetails$LoadAllEvents$4$2.class
* out\production\Feast Of Fest\com\example\Fest\EventsDetails$LoadAllEvents$4.class
* out\production\Feast Of Fest\com\example\Fest\EventsDetails$LoadAllEvents.class
* out\production\Feast Of Fest\com\example\Fest\EventsDetails.class
* out\production\Feast Of Fest\com\example\Fest\Final_Help_Screen$1.class
* out\production\Feast Of Fest\com\example\Fest\Final_Help_Screen.class
* out\production\Feast Of Fest\com\example\Fest\Help_Screen1$1.class
* out\production\Feast Of Fest\com\example\Fest\Help_Screen1.class
* out\production\Feast Of Fest\com\example\Fest\Help_Screen2$1.class
* out\production\Feast Of Fest\com\example\Fest\Help_Screen2.class
* out\production\Feast Of Fest\com\example\Fest\Help_Screen3$1.class
* out\production\Feast Of Fest\com\example\Fest\Help_Screen3.class
* out\production\Feast Of Fest\com\example\Fest\JSONParser.class
* out\production\Feast Of Fest\com\example\Fest\MyActivity$1.class
* out\production\Feast Of Fest\com\example\Fest\MyActivity.class
* out\production\Feast Of Fest\com\example\Fest\MyApplication.class
* out\production\Feast Of Fest\com\example\Fest\MyService$LoadCurrentEvents.class
* out\production\Feast Of Fest\com\example\Fest\MyService.class
* out\production\Feast Of Fest\com\example\Fest\NestedListView.class
* out\production\Feast Of Fest\com\example\Fest\OnBackPressedListener.class
* out\production\Feast Of Fest\com\example\Fest\Publish_At_Us$1.class
* out\production\Feast Of Fest\com\example\Fest\Publish_At_Us.class
* out\production\Feast Of Fest\com\example\Fest\R$anim.class
* out\production\Feast Of Fest\com\example\Fest\R$attr.class
* out\production\Feast Of Fest\com\example\Fest\R$color.class
* out\production\Feast Of Fest\com\example\Fest\R$drawable.class
* out\production\Feast Of Fest\com\example\Fest\R$id.class
* out\production\Feast Of Fest\com\example\Fest\R$layout.class
* out\production\Feast Of Fest\com\example\Fest\R$menu.class
* out\production\Feast Of Fest\com\example\Fest\R$string.class
* out\production\Feast Of Fest\com\example\Fest\R$style.class
* out\production\Feast Of Fest\com\example\Fest\R$xml.class
* out\production\Feast Of Fest\com\example\Fest\R.class
* out\production\Feast Of Fest\com\example\Fest\RegisterEvent$1.class
* out\production\Feast Of Fest\com\example\Fest\RegisterEvent$RegisterEvents.class
* out\production\Feast Of Fest\com\example\Fest\RegisterEvent.class
* out\production\Feast Of Fest\com\example\Fest\ScrollViewInterceptor.class
* out\production\Feast Of Fest\com\example\Fest\Splash$1.class
* out\production\Feast Of Fest\com\example\Fest\Splash$2.class
* out\production\Feast Of Fest\com\example\Fest\Splash$3.class
* out\production\Feast Of Fest\com\example\Fest\Splash$4.class
* out\production\Feast Of Fest\com\example\Fest\Splash.class
* out\production\Feast Of Fest\com\example\Fest\TabsPagerAdapter.class
* out\production\Feast Of Fest\com\example\Fest\myClickableSpan.class
* out\production\Fest\
* out\production\Fest\Fest.apk
* out\production\Fest\Fest.unaligned.apk
* out\production\Fest\com\
* out\production\Fest\com\example\
* out\production\Fest\com\example\Fest\
* out\production\Fest\com\example\Fest\About_Us$1.class
* out\production\Fest\com\example\Fest\About_Us$2.class
* out\production\Fest\com\example\Fest\About_Us.class
* out\production\Fest\com\example\Fest\All_Events$LoadAllEvents$1$1.class
* out\production\Fest\com\example\Fest\All_Events$LoadAllEvents$1$2.class
* out\production\Fest\com\example\Fest\All_Events$LoadAllEvents$1.class
* out\production\Fest\com\example\Fest\All_Events$LoadAllEvents$2.class
* out\production\Fest\com\example\Fest\All_Events$LoadAllEvents$3$1.class
* out\production\Fest\com\example\Fest\All_Events$LoadAllEvents$3$2.class
* out\production\Fest\com\example\Fest\All_Events$LoadAllEvents$3.class
* out\production\Fest\com\example\Fest\All_Events$LoadAllEvents.class
* out\production\Fest\com\example\Fest\All_Events.class
* out\production\Fest\com\example\Fest\BuildConfig.class
* out\production\Fest\com\example\Fest\Current_Events$LoadCurrentEvents$1$1.class
* out\production\Fest\com\example\Fest\Current_Events$LoadCurrentEvents$1$2.class
* out\production\Fest\com\example\Fest\Current_Events$LoadCurrentEvents$1.class
* out\production\Fest\com\example\Fest\Current_Events$LoadCurrentEvents$2.class
* out\production\Fest\com\example\Fest\Current_Events$LoadCurrentEvents$3$1.class
* out\production\Fest\com\example\Fest\Current_Events$LoadCurrentEvents$3$2.class
* out\production\Fest\com\example\Fest\Current_Events$LoadCurrentEvents$3.class
* out\production\Fest\com\example\Fest\Current_Events$LoadCurrentEvents.class
* out\production\Fest\com\example\Fest\Current_Events.class
* out\production\Fest\com\example\Fest\EventsDetails$DownloadImageTask.class
* out\production\Fest\com\example\Fest\EventsDetails$LoadAllEvents$1.class
* out\production\Fest\com\example\Fest\EventsDetails$LoadAllEvents$2.class
* out\production\Fest\com\example\Fest\EventsDetails$LoadAllEvents$3.class
* out\production\Fest\com\example\Fest\EventsDetails$LoadAllEvents$4$1$1.class
* out\production\Fest\com\example\Fest\EventsDetails$LoadAllEvents$4$1.class
* out\production\Fest\com\example\Fest\EventsDetails$LoadAllEvents$4.class
* out\production\Fest\com\example\Fest\EventsDetails$LoadAllEvents.class
* out\production\Fest\com\example\Fest\EventsDetails.class
* out\production\Fest\com\example\Fest\Final_Help_Screen$1.class
* out\production\Fest\com\example\Fest\Final_Help_Screen.class
* out\production\Fest\com\example\Fest\Help_Screen1$1.class
* out\production\Fest\com\example\Fest\Help_Screen1.class
* out\production\Fest\com\example\Fest\Help_Screen2$1.class
* out\production\Fest\com\example\Fest\Help_Screen2.class
* out\production\Fest\com\example\Fest\Help_Screen3$1.class
* out\production\Fest\com\example\Fest\Help_Screen3.class
* out\production\Fest\com\example\Fest\JSONParser.class
* out\production\Fest\com\example\Fest\MyActivity$1.class
* out\production\Fest\com\example\Fest\MyActivity.class
* out\production\Fest\com\example\Fest\MyApplication.class
* out\production\Fest\com\example\Fest\NonScrollListView.class
* out\production\Fest\com\example\Fest\Publish_At_Us$1.class
* out\production\Fest\com\example\Fest\Publish_At_Us$2.class
* out\production\Fest\com\example\Fest\Publish_At_Us.class
* out\production\Fest\com\example\Fest\R$anim.class
* out\production\Fest\com\example\Fest\R$attr.class
* out\production\Fest\com\example\Fest\R$drawable.class
* out\production\Fest\com\example\Fest\R$id.class
* out\production\Fest\com\example\Fest\R$layout.class
* out\production\Fest\com\example\Fest\R$menu.class
* out\production\Fest\com\example\Fest\R$string.class
* out\production\Fest\com\example\Fest\R.class
* out\production\Fest\com\example\Fest\RegisterEvent$1.class
* out\production\Fest\com\example\Fest\RegisterEvent$RegisterEvents.class
* out\production\Fest\com\example\Fest\RegisterEvent.class
* out\production\Fest\com\example\Fest\Splash$1.class
* out\production\Fest\com\example\Fest\Splash$2.class
* out\production\Fest\com\example\Fest\Splash$3.class
* out\production\Fest\com\example\Fest\Splash$4.class
* out\production\Fest\com\example\Fest\Splash.class
* out\production\Fest\com\example\Fest\myClickableSpan.class
* proguard_logs\
* proguard_logs\dump.txt
* proguard_logs\mapping.txt
* proguard_logs\seeds.txt
* proguard_logs\usage.txt

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => app\src\main\AndroidManifest.xml
* assets\ => app\src\main\assets\
* libs\acra-4.6.2-javadoc.jar => app\libs\acra-4.6.2-javadoc.jar
* libs\acra-4.6.2-sources.jar => app\libs\acra-4.6.2-sources.jar
* libs\acra-4.6.2.jar => app\libs\acra-4.6.2.jar
* proguard-project.txt => app\proguard-project.txt
* res\ => app\src\main\res\
* src\ => app\src\main\java\

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)

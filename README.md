# FEAST OF FEST #

### What is this repository for? ###

* Quick summary
* Outlook of the app
* [Play Store Link ](https://play.google.com/store/apps/details?id=com.androsemantic.fest)

### Summary ###
Feast Of Fest is one of its kind application which provides details about various fests in and around the City.
It also lets you to register to fest and get in contact with the organizer directly. With over 1000+ downloads and new feature requests coming in we are working hard to keep this application useful to one and all.

### Outlook of the APP ###

![appImage.png](https://bitbucket.org/repo/B7RgLG/images/2229504819-appImage.png)

![device-2015-10-03-225933.png](https://bitbucket.org/repo/B7RgLG/images/1837575489-device-2015-10-03-225933.png)

![device-2015-11-29-211915.png](https://bitbucket.org/repo/B7RgLG/images/2543427864-device-2015-11-29-211915.png)

### Who do I talk to? ###

* Srinivas R Kulkarni - Srinivasrk92@gmail.com